<p><img src="http://shivang.comuf.com/scode/sc.jpg" alt="Smart Code" width="290"/></p>
<h2><a href="https://gitlab.com/sivu503/scode"></a></h2>
<h2>About</h2>
<p>This Plugin is for popup. this will help you to maintain manny popup's at same page and also set color of overlay background and other functionality</p>
<h2>Usage / example</h2>
<p>(1)<code>data-popup="1"</code></p>
<p>(2)<code>data-pop_open="1"</code></p>
<p>(3)<code>data-pop="close"</code></p>
<p>(4)<code>$('selector').popup({overlay:true,overlayColor:'rgba(0,0,0,0.7)',});</code></p>
<br>
<p>Example is in Repository
<h2>Author</h2>
<p>Shivang Pokar <a href="https://gitlab.com/sivu503/">https://gitlab.com/sivu503/</a></p>


