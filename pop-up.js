/*
* Title: Jquery PopUp System
* Author: Shivang Pokar
* Author URL:
* Version: 1.0.1
* Copyright reserve by shivang
*/

;(function($){
    $.fn.popup = function(options){

        /*========= Defaults =========*/
        var defaults = {
            overlay:false,
            overlayColor:'rgba(0,0,0,0.8)',
            overlayClick:'close',

        };
        var options = $.extend(defaults, options);
        return this.each(function() {

            /* Variables
================================================== */
            var $this = $(this);
            var $pop_open  = $('.pop_open');
            var $popup_no = $('*').data('popup');
            var $popup_btnNo = $('*').data('pop_open');
            var $close = $('*').data('pop');
            var $overlay_color = options.overlaycolor;
            var $popopen = $('*[data-pop_open]');

            /* Overlay 
=================================================== */
            if(options.overlay===true){
                if($('body').find('.overlay').length > 0){
                    
                }
                else{
                    $('body').append('<div class="overlay" style="background-color:'+options.overlayColor+'; left:0; right:0; top:0; bottom:0; position:fixed; display:none; "></div>');   
                }
            }
            /*else{
                $this.children().remove('.overlay');
            }*/

            /* Click To Open POP Up 
==================================================== */
            $popopen.click(function(){
                var thispopbtn = $(this).data('pop_open');
                var thispopup = $('*[data-popup='+ thispopbtn +']');
                thispopup.addClass('active');
                $('.overlay').addClass('active');
                $('.overlay').fadeIn();
            });

            /* Click to Close POP UP
==================================================== */
            $('[data-pop="close"]').click(function(){
                $('*[data-popup]').removeClass('active');
                $overlay.removeClass('active');
                $('.overlay').fadeOut();
            });

            var $overlay = $('.overlay');

            /* When Clicking on overlay and close the POP UP
===================================================== */
            if(options.overlayClick==="close")
            {
                $('.overlay').click(function(){
                    $('*[data-popup]').removeClass('active');
                    $overlay.removeClass('active');
                    $('.overlay').fadeOut();
                });
            }
        });
    }

})(jQuery);